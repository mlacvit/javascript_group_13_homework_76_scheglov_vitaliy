export class MessModel {
  constructor(
    public id: string,
    public user: string,
    public text: string,
    public date: string,
  ) {}
}

export interface MessData {
  user: string,
  text: string,
}
