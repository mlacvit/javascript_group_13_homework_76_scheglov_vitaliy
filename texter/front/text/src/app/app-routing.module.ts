import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllmessComponent } from './pages/allmess/allmess.component';
import { NewmessComponent } from './pages/newmess/newmess.component';
import { MessoneComponent } from './pages/messone/messone.component';

const routes: Routes = [
  {path: '', component: AllmessComponent},
  {path: 'message/new', component: NewmessComponent},
  {path: 'message/:id', component: MessoneComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
