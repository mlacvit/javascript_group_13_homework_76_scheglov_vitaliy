import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessData, MessModel } from './mess.model';
import { Subject, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MessserviceService {
  messages: MessModel[] | null = null;
  message!: MessModel | null;
  messChange = new Subject<MessModel[]>();
  fetching = new Subject<boolean>();
  fetchMess = new Subject<boolean>();
  fetchMessOne = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) {
  }

  getMessages() {
    this.fetching.next(true);
    return this.http.get<MessModel[]>(environment.apiUrl + '/message').pipe(
      map(response => {
        return response.map(mess => {
          return new MessModel(
            mess.id,
            mess.user,
            mess.text,
            mess.date
          );
        });
      })
    ).subscribe((mess: MessModel[]) => {
      this.messages = mess;
      this.messChange.next(this.messages.slice());
      this.fetching.next(false);
    }, error => {
      this.fetching.next(false);
    });
  }

  createMess(messData: MessData) {
    this.fetchMess.next(true);
    return this.http.post(environment.apiUrl + '/message/', messData).pipe(
      tap(() => {
        this.fetchMess.next(false);
      }, error => {
        this.fetchMess.next(false);
      })
    );
  }

  getOne(id: string) {
    this.fetchMessOne.next(true);
    return this.http.get<MessModel>(environment.apiUrl + `/message/${id}`)
      .pipe(map(result => {
        if (result === null) {
          return null;
        }
        return new MessModel(
          id,
          result.user,
          result.text,
          result.date,
        );
      })).subscribe((mess) => {
        this.message = mess;
        this.fetchMessOne.next(false);
      });
  };
}
