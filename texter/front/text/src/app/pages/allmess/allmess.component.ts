import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessModel } from '../../mess.model';
import { MessserviceService } from '../../messservice.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-allmess',
  templateUrl: './allmess.component.html',
  styleUrls: ['./allmess.component.sass']
})
export class AllmessComponent implements OnInit, OnDestroy {
  messages: MessModel[] = [];
  changeSub!: Subscription;
  fetchSub!: Subscription;
  isFetching: boolean = false;
  constructor(private services: MessserviceService) { }

  ngOnInit(): void {
    this.fetchSub = this.services.fetching.subscribe( (isFech: boolean) => {
      this.isFetching = isFech;
    });

      this.services.getMessages();
      this.changeSub = this.services.messChange.subscribe( mess => {
      this.messages = mess;
    });

  }

  ngOnDestroy(): void {
    this.changeSub.unsubscribe();
    this.fetchSub.unsubscribe();
  }

}
