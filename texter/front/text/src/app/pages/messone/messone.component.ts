import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessModel } from '../../mess.model';
import { MessserviceService } from '../../messservice.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messone',
  templateUrl: './messone.component.html',
  styleUrls: ['./messone.component.sass']
})
export class MessoneComponent implements OnInit, OnDestroy {
  fetchSubOne!: Subscription;
  isFechOne: boolean = false;
  constructor(public service: MessserviceService, private rout: ActivatedRoute) { }

  ngOnInit(): void {
    this.fetchSubOne = this.service.fetchMessOne.subscribe( (isFech: boolean) => {
      this.isFechOne = isFech;
    });

    this.rout.params.subscribe((params: Params) => {
     const data = params['id'];
      this.service.getOne(data);
    })
  }

  ngOnDestroy(): void {
    this.fetchSubOne.unsubscribe();
  }

}
