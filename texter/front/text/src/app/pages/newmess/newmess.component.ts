import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessserviceService } from '../../messservice.service';
import { MessData } from '../../mess.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-newmess',
  templateUrl: './newmess.component.html',
  styleUrls: ['./newmess.component.sass']
})
export class NewmessComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  fetchingSubscriptions!: Subscription;
  isFetching: boolean = false;
  constructor(
    private service: MessserviceService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.fetchingSubscriptions = this.service.fetchMess.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
  }

  onSub() {
    const value: MessData = this.form.value;
    this.service.createMess(value).subscribe(() => {
    void this.router.navigate(['/']);
    });
  }

  ngOnDestroy(): void {
    this.fetchingSubscriptions.unsubscribe();
  }

}
