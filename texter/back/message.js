const express = require('express');
const router = express.Router();
const file = require('./files');

router.get('/', (req, res) => {
  const message = file.getItems();
  res.send(message.slice(30));
});

router.get('/:id', (req, res) => {
  const message = file.getItem(req.params.id);
  if (!message) {
    res.status(404).send({message: 'not found'})
  }
  return res.send(message);
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.user || !req.body.text) {
      return res.status(404).send({message: 'user & text are required!'});
    }
    const message = {
      user: req.body.user,
      text: req.body.text,
    };
    await file.addItem(message);

    return res.send({message: 'created new message', id: message.id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;