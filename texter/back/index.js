const express = require('express');
const app = express();
const file = require('./files');
const message = require('./message');
const cors = require('cors');
const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/message', message);

const run =async () => {
 await file.init();
}


app.listen(port, () => {
  console.log('We are live on ' + port);
});

run().catch(e => console.error(e));